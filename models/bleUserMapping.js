const _ = require('lodash');
const Sequalize = require('sequelize');
const sequelize = require('./../config/database');


const bleUserMapping = sequelize.define('bleUserMapping', {
    mapping_id: {
        type: Sequalize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    
    }, ble_id: {
        type: Sequalize.INTEGER
    },
    user_id: {
        type: Sequalize.INTEGER
    },ble_left_user: {
        type: Sequalize.STRING,
        defaultValue : "LeftUser"
    },
    ble_right_user: {
        type: Sequalize.STRING,
        defaultValue : "RightUser"
    }
});

module.exports = bleUserMapping;