const _ = require('lodash');
const Sequalize = require('sequelize');
const sequelize = require('./../config/database');


const ble_details = sequelize.define('ble_details', {
    ble_id: {
        type: Sequalize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    
    }, ble_address: {
        type: Sequalize.STRING
    },
    ble_bootstrapping_status: {
        type: Sequalize.STRING
    },
    ble_left_user: {
        type: Sequalize.STRING,
        defaultValue : "LeftUser"
    },
    ble_right_user: {
        type: Sequalize.STRING,
        defaultValue : "RightUser"
    },
    user_id:{
        type: Sequalize.INTEGER
    }
});

module.exports = ble_details;