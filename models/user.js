const _ = require('lodash');
const Sequalize = require('sequelize');
const sequelize = require('./../config/database');


const user = sequelize.define('user_details', {
    user_id: {
        type: Sequalize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    
    },
    password: {
        type: Sequalize.STRING,
        allowNull: true,
    },
     address: {
        type: Sequalize.TEXT,
        allowNull: true,
    },
    country: {
        type: Sequalize.STRING,
        allowNull: true,
    },
    state: {
        type: Sequalize.STRING,
        allowNull: true,
    },
    city: {
        type: Sequalize.STRING,
        allowNull: true,
    },
    pincode: {
        type: Sequalize.BIGINT,
        allowNull: true,
    },
    mobileNo: {
        type: Sequalize.STRING,
        allowNull: true,
    },
    gender:{
        type: Sequalize.STRING,
        allowNull: true
    },

    user_status:{
        type: Sequalize.BOOLEAN,
        allowNull: true
    },
    emailId:{
        type: Sequalize.STRING,
        allowNull: true
    },
    full_name:{
        type: Sequalize.STRING,
        allowNull: true
    },

    chamber_type:{
        type: Sequalize.STRING,
        allowNull: true
    },
    ble_id:{
        type: Sequalize.INTEGER,
        allowNull: true
    }

});

module.exports = user;