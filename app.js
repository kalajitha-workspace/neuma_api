
require('./config/config');

const express = require('express');
const Sequelize = require('sequelize');
const _ = require('lodash');
const bodyParser = require('body-parser');
const sequalize = require('./config/database');
const app = express();
const timeout = require('connect-timeout');
const port = 3000;
const helmet = require('helmet');
const sequelize = require('./config/database');
var moment = require('moment');
const axios = require('axios')
app.use(bodyParser.json());
app.use(timeout(300000));
app.use(express.static('public'));
app.use(helmet());
var cors = require('cors');
var device = require('express-device');
const userDetails = require('./models/user.js')
const ble_details = require('./models/ble_details.js');
const bleUserMapping = require('./models/bleUserMapping.js');


app.get('/testapi',async(req,res)=>{
    res.send("working")
})

app.post('/checkMobileExists', async(req, res) => {
    try{
        var body = _.pick(req.body,['mobileNo']);
   
       var user1 = await userDetails.findOne({where:{
        mobileNo :body.mobileNo
       }})
        console.log("user1"+JSON.stringify(user1));
        if(user1){
           var msg =  {message: "Mobile number already exists, Please Sign In",status:"202", userDetails: user1};
           res.status(200).send(msg)
        }
        else{
            var msg =  {message: "Mobile number not registered",status:"201", userDetails: null};
            res.status(200).send(msg)
        }
    }
    catch(e){
        console.log(e);
        res.status(500).send(e)
    }
});

app.get('/cityList',async(req,res)=>{
        var query1 =  `select * from cities`;
        var city = await sequalize.query(query1, { type: Sequelize.QueryTypes.SELECT });
        res.status(200).send(city)
})

app.post('/checkemailIdExists', async(req, res) => {
    try{
        var body = _.pick(req.body,['emailId']);
   
       var user1 = await userDetails.findOne({where:{
        emailId :body.emailId
       }})
        console.log("user1"+JSON.stringify(user1));
        if(user1){
           var msg =  {message: "email Id already exists, Please Sign In",status:"202", userDetails: user1};
           res.status(200).send(msg)
        }
        else{
            var msg =  {message: "email Id not registered",status:"201", userDetails: null};
            res.status(200).send(msg)
        }
    }
    catch(e){
        console.log(e);
        res.status(500).send(e)
    }
});

app.post('/updateBootstarppingStatus', async(req, res) => {
    try{
        var body = _.pick(req.body,['user_id','ble_address','ble_bootstrapping_status']);
   
    //    await userDetails.findOne({where:{
    //     mobileNo:body.mobileNo
    //    }}).then(async(user1)=>{
    //     user1.bootstrapping_status = body.bootstrapping_status;
    //     user1.save();
    await ble_details.findOne({where:{
        ble_address : body.ble_address
    }}).then(re1=>{
        re1.user_id = body.user_id
        re1.ble_bootstrapping_status = body.ble_bootstrapping_status
        re1.save();
   // })
        var msg =  {message: "bootstrapping_status updated successfully",status:"201"};
           res.status(200).send(msg)
       })
      
         
    }
    catch(e){
        console.log(e);
        res.status(500).send(e)
    }
});

app.post('/bledetailsInsertUpdate',async(req,res)=>{
    try{
    var body = _.pick(req.body,['ble_address','user_id','ble_bootstrapping_status']);
    var ble = await ble_details.findOne({
        where:{
            ble_address:body.ble_address
        }
    })
   
    if(ble){
        ble.ble_address = body.ble_address;
        ble.user_id = body.user_id;
        ble.ble_bootstrapping_status = body.ble_bootstrapping_status;
        ble.save();
        await bleUserMapping.findOne({where:{
            user_id : body.user_id
        }
        }).then(re=>{
            if(re){
                re.ble_id = ble.ble_id;
                re.save();
            }
            else{
                var map = new bleUserMapping();
                map.user_id = body.user_id;
                map.ble_id = ble.ble_id;
                map.save();
            }
        })
        var msg =  {message: "Ble details updated successfully",status:"201"};
        res.status(200).send(msg)
    }
    else{
        var ble1  = new ble_details(body);
        ble1.save();
        await bleUserMapping.findOne({where:{
            user_id : body.user_id
        }
        }).then(re=>{
            if(re){
                re.ble_id = ble1.ble_id;
                re.save();
            }
            else{
                var map = new bleUserMapping();
                map.user_id = body.user_id;
                map.ble_id = ble1.ble_id;
                map.save();
            }
        })
        var msg =  {message: "Ble details inserted successfully",status:"201"};
        res.status(200).send(msg)
    }
  
}
catch(err){
    console.log(err);
    var msg =  {message: "Something went wrong",status:"500"};
    res.status(200).send(msg)
}
})  

app.post('/updateLeftRightUsers',async(req,res)=>{
    try{
    var body = _.pick(req.body,['ble_address','ble_left_user','ble_right_user','user_id']);
    var ble = await ble_details.findOne({
        where:{
            ble_address:body.ble_address
        }
    })
    if(ble){
        await bleUserMapping.findOne({where:{
            user_id:body.user_id
        }}).then(re=>{
            if(re){
            re.ble_left_user=body.ble_left_user;
            re.ble_right_user=body.ble_right_user
            re.ble_address = body.ble_address
            re.save();
            }
            else{
                var map = new bleUserMapping(body);
                map.save();
            }
        })
            ble.ble_left_user = body.ble_left_user;
            ble.ble_right_user = body.ble_right_user;
            ble.save();
            var msg =  {message: "User details updated successfully",status:"201"};
            res.status(200).send(msg)
       
      
    }
   
}
catch(err){
    console.log(err);
    var msg =  {message: "Something went wrong",status:"500"};
    res.status(200).send(msg)
}
}) 

app.post('/viewBleDetailsByUserId',async(req,res)=>{
    try{
    var body = _.pick(req.body,['user_id']);
    await bleUserMapping.findOne({
        where:{
            user_id: body.user_id
        }
    }).then(async(re)=>{
        if(re){
            
            await ble_details.findOne({where:{
                ble_id : re.ble_id
            }}).then(re=>{
              
            var msg =  {message: "details fetched",status:"201", userDetails: re};
            res.status(200).send(msg)
              
        })
        }
        else{
            var msg =  {message: "Controller not found",status:"202", userDetails: null};
            res.status(200).send(msg)
        }
    })
    }
    catch(err){
        console.log(err);
        var msg =  {message: "Something went wrong",status:"500"};
        res.status(200).send(msg)
    }
});

app.post('/updateBleToUser',async(req,res)=>{
    try{
    var body = _.pick(req.body,['mobileNo','ble_address']);
    var ble = await ble_details.findOne({
        where:{
            ble_address:body.ble_address
        }
    })
    if(ble){
        await userDetails.findOne({where:{
            mobileNo:body.mobileNo
        }}).then(re=>{
            re.ble_id = ble.ble_id;
            re.save();
            var msg =  {message: "Ble address updated successfully",status:"201"};
            res.status(200).send(msg)
        })
      
    }
    else{
        var ble1  = new ble_details(body);
        ble1.save().then(re=>{
            console.log(re);
        });
        var ble2 = await ble_details.findOne({
            where:{
                ble_address:body.ble_address
            }
        })
        console.log("ble2"+ble2);
        await userDetails.findOne({where:{
            mobileNo:body.mobileNo
        }}).then(re=>{
            re.ble_id = ble2.ble_id;
            var msg =  {message: "Ble address updated successfully",status:"201"};
            res.status(200).send(msg)
        })
    }
  
}
catch(err){
    console.log(err);
    var msg =  {message: "Something went wrong",status:"500"};
    res.status(200).send(msg)
}
}) 

app.post('/viewBleDetails',async(req,res)=>{
    try{
    var body = _.pick(req.body,['ble_address']);
    await ble_details.findOne({
        where:{
            ble_address: body.ble_address
        }
    }).then(re=>{
        if(re){
         if(!re.ble_left_user){
             re.ble_left_user="LeftUser"
         }
         if(!re.ble_right_user){
            re.ble_right_user="RightUser"
        }
            var msg =  {message: "details fetched",status:"201", userDetails: re};
            res.status(200).send(msg)
        }
        else{
            var msg =  {message: "Controller not found",status:"202", userDetails: mailCheck};
            res.status(200).send(msg)
        }
    })
    }
    catch(err){
        console.log(err);
        var msg =  {message: "Something went wrong",status:"500"};
        res.status(200).send(msg)
    }
});
app.post('/signup',async(req,res)=>{
    try{
    var body = _.pick(req.body,['mobileNo','emailId','address','fullName','password']);
    var user = await userDetails.findOne({
        where:{
            mobileNo:body.mobileNo
        }
    })
    var mailCheck = await userDetails.findOne({
        where:{
            emailId:body.emailId
        }
    })
    console.log("user"+JSON.stringify(user));
    if(user){
        var msg =  {message: "Mobile number is already registered",status:"202", userDetails: user};
        res.status(200).send(msg)
    }
    else if(mailCheck){
        var msg =  {message: "Email Id already registered",status:"203", userDetails: mailCheck};
        res.status(200).send(msg)
    }
    else{
        var user2 = new userDetails(body);
       await user2.save().then(re=>{
         
        var msg =  {message: "User registered successfully",status:"201", userDetails: user};
        res.status(200).send(msg)
           
    })
    }
}
catch(err){
    console.log(err);
    var msg =  {message: "Something went wrong",status:"500"};
    res.status(200).send(msg)
}
})       

app.post('/login',async(req,res)=>{
    try{
    var body = _.pick(req.body,['mobileNo','password']);
    var user = await userDetails.findOne({
        where:{
            mobileNo:body.mobileNo
        }
    })
    if(user){
        await userDetails.findOne({
            where:{
                mobileNo:body.mobileNo,
                password:body.password
            }
        }).then(re=>{
            if(re){
            var msg =  {message: "Authenticated successfully",status:"201", userDetails: re};
            res.status(200).send(msg)
            }
            else{
                var msg =  {message: "incorrect password",status:"203", userDetails: null};
                res.status(200).send(msg)
            }
        })
        
    }
   
    else{
       
        var msg =  {message: "Mobile Number not exist",status:"202", userDetails: user};
        res.status(200).send(msg)
           
    
    }
}
catch(err){
    console.log(err);
    var msg =  {message: "Something went wrong",status:"500"};
    res.status(200).send(msg)
}
}) 


app.post('/updateFullname', async(req, res) => {
    try{
        var body = _.pick(req.body,['user_ID','fullName']);
        
        var query1 =  ` update neuma1.user_details set full_name = '${body.fullName}' where user_id = ${body.user_ID}`;
        var user1 = await sequalize.query(query1, { type: Sequelize.QueryTypes.SELECT });
        console.log("user1"+JSON.stringify(user1));

           var msg =  {message: "username updated successfully",status:"201", userDetails: user1};
           res.status(200).send(msg)
      
       
    }
    catch(e){
        console.log(e);
        var msg =  {message: "username updation failed",status:"202", userDetails: user1};
        res.status(200).send(msg)
    }
});

app.post('/updateEmailId', async(req, res) => {
    try{
        var body = _.pick(req.body,['user_ID','emailId']);
        
        var query1 =  ` update neuma1.user_details set email_id = '${body.emailId}' where user_id = ${body.user_ID}`;
        var user1 = await sequalize.query(query1, { type: Sequelize.QueryTypes.SELECT });
        console.log("user1"+JSON.stringify(user1));

           var msg =  {message: "Email Id updated successfully",status:"201", userDetails: user1};
           res.status(200).send(msg)
      
       
    }
    catch(e){
        console.log(e);
        var msg =  {message: "Email Id updation failed",status:"202", userDetails: user1};
        res.status(200).send(msg)
    }
});





app.listen(port, () => {
    sequelize.sync();
console.log(`listining on port ${port}`);
});
         